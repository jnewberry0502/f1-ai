import pathlib
import time

from f12019.telemetry import Telemetry
import pickle
from datetime import datetime

if __name__ == '__main__':
    captures_dir = pathlib.Path(__file__).parent / "captures"
    captures_dir.mkdir(exist_ok=True)

    telem = Telemetry()

    i = 0
    while True:
        telem_data = []
        capture_file = captures_dir / (str(datetime.now().strftime("%Y%m%d-%H%M%S")) + f"_{i}.telem")
        while len(telem_data) < 10000:

            for event in telem.get():
                telem_data.append(event)

            time.sleep(0.01)

        with open(str(capture_file), "wb") as f:
            pickle.dump(telem_data, f)

        i += 1


