import pickle
from datetime import datetime

from f12019.telemetry import Location


class DummyController:
    def __init__(self):
        self.steering = 0
        self.throttle = 0
        self.brake = 0

class TelemetryLoader:
    def __init__(self, capture_dir):
        self.start_time = None
        self.capture_dir = capture_dir

        self.events = []

        for file in list(capture_dir.glob("*.telem")):
            self.events.extend(pickle.load(open(file, 'rb')))

        self.events = self.events[175:]

        self.i = 0
        self.capture_start_time = self.events[0].recieved

    @property
    def init_location(self):
        for event in self.events:
            if isinstance(event, Location):
                return event
        return None

    def get(self):
        if self.start_time is None:
            self.start_time = datetime.now()

        events = []

        while self.i < len(self.events) and (datetime.now() - self.start_time) > (self.events[self.i].recieved - self.capture_start_time):
            events.append(self.events[self.i])
            self.i += 1

        return events