from f12019.telemetry import LapData, Location, Telemetry
from render import Render
from track import Track

last_dist = 5000


def wait_for_new_lap(telem, render):
    global last_dist
    new_lap = False
    while True:
        for event in telem.get():
            if isinstance(event, LapData):
                if event.dist_travelled > 0 and (last_dist - event.dist_travelled) > 1000:
                    # New lap detected
                    new_lap = True
                last_dist = event.dist_travelled
                if last_dist < 0:
                    last_dist = 5000
                if new_lap:
                    return
            yield event

        render.render()


def log_points(events):
    for event in events:
        if isinstance(event, Location):
            yield event
            render.view_pos = (event.x, event.z)


if __name__ == "__main__":

    track = Track("test.npz")

    render = Render(60, [track], 0.4)

    telem = Telemetry()

    print("Cross the line, and then drive around the outside of the track")

    for x in wait_for_new_lap(telem, render):
        pass

    print("---\n---\n---\nCrossed the line: Starting outside recording")

    for loc in log_points(wait_for_new_lap(telem, render)):
        track.add_outer_point(loc)

    print("---\n---\n---\nCross the line, and then drive around the inside of the track")

    for x in wait_for_new_lap(telem, render):
        pass

    print("---\n---\n---\nCrossed the line: Starting inside recording")

    for loc in log_points(wait_for_new_lap(telem, render)):
        track.add_inner_point(loc)

    track.save()
