import torch.nn as nn

NUM_INPUTS = 5
NUM_OUTPUTS = 3


class DQN(nn.Module):

    def __init__(self, h, w, outputs):
        super(DQN, self).__init__()
        self.layer = nn.Sequential(
            nn.Linear(NUM_INPUTS, 10),
            nn.Linear(10, NUM_OUTPUTS)
        )

    def forward(self, x):
        x = self.layer(x)
        return x
