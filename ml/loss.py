import numpy as np
import os


class Loss:

    def __init__(self):
        self.dist_travelled = 0.0
        self.time_elapsed = 0.0
        self.lap_invalidated = False
        self.track_length = 1.0
        self.avg_speed_max = -1.0

    def set_track_length(self, track_length):
        self.track_length = track_length

    def set_dist_travelled(self, dist_travelled):
        if self.lap_invalidated:
            return

        self.dist_travelled = dist_travelled

    def set_time_elapsed(self, time_elapsed):
        if self.lap_invalidated:
            return

        self.time_elapsed = time_elapsed

    def set_lap_invalidated(self, lap_invalidated):
        self.lap_invalidated = lap_invalidated

    def calculate_loss(self):
        if self.time_elapsed == 0 or self.track_length == 0 or self.dist_travelled < 0.0:
            return -100.0

        lap_percentage = self.dist_travelled / self.track_length
        penalty = -1.0 if self.lap_invalidated else 0.0
        avg_speed = (self.dist_travelled / self.time_elapsed) / 1000.0
        fitness = (lap_percentage + avg_speed) - penalty
        loss = 1.25 - fitness
        return loss

    #def torch_loss
