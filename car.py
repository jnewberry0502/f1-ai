import numpy as np
import pygame

from track import point2surface


class Car:
    def __init__(self, pos=(0,0)):
        self.pos = pos
        self.yaw = 0

    def render(self, surface, view_pos, view_scale, render_size, **kwargs):
        center = point2surface(self.pos[0], self.pos[1], view_pos, view_scale, render_size)
        pygame.draw.circle(surface, (255, 0, 0), center, 7)

        offset = 20
        line_length = 20

        tail = center + np.array([
            offset * np.cos(self.yaw),
            offset * -np.sin(self.yaw)
        ])

        head = center + np.array([
            (line_length + offset) * np.cos(self.yaw),
            (line_length + offset) * -np.sin(self.yaw)
        ])

        pygame.draw.line(surface, (0, 255, 0), head, tail, 2)