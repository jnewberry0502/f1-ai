import pathlib
from datetime import datetime

import pygame

from bots.pid import PIDBot
from car import Car
from info import Info
from render import Render
from simulation.car import clamp
from simulation.controller import SimulationController
from simulation.simulation import Simulation
from simulation.telemetry import SimulationTelemetry
from f12019.telemetry import Location, LapData, TrackInfo, Telemetry, CarData, CarTelemetry
from telemetry_loader import TelemetryLoader, DummyController
from track import Track
from lidar import Lidar
import numpy as np
from ml.loss import Loss

loss_func = Loss()

track = Track("australia.npz")
track.load()
track.simplify(epsilon=0.35)
track.save()

lidar = Lidar(track, num_sensors=20, fov_degrees=180)

ENV = "replay"

if ENV == "sim":
    sim = Simulation(track)
    telem = SimulationTelemetry(sim)
    controller = SimulationController(sim)

elif ENV == "f12019":
    from f12019.controller import Controller
    telem = Telemetry()
    controller = Controller()

elif ENV == "replay":
    telem = TelemetryLoader(pathlib.Path(__file__).parent / "captures")
    controller = DummyController()

car = Car()
info = Info()
fps = 60
render = Render(fps, [track, lidar, car, info], 0.1)

bot = PIDBot()


while True:

    render.view_pos = car.pos

    events = render.event_loop()
    render.render()

    bot_data = {
        "events": events
    }

    for event in events:
        if event.type == pygame.KEYDOWN or event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                controller.throttle = 1 if event.type == pygame.KEYDOWN else 0
            if event.key == pygame.K_s:
                controller.brake = 1 if event.type == pygame.KEYDOWN else 0
            if event.key == pygame.K_a:
                controller.steering += -1 if event.type == pygame.KEYDOWN else 1
            if event.key == pygame.K_d:
                controller.steering += 1 if event.type == pygame.KEYDOWN else -1

    for event in telem.get():
        if isinstance(event, Location):
            car.pos = event.x, event.z
            car.yaw = event.yaw - np.pi / 2

            lidar.generate_intersection_points(car.pos, car.yaw)

            info.location = event

        elif isinstance(event, LapData):
            loss_func.set_lap_invalidated(not event.valid)
            loss_func.set_time_elapsed(event.current_lap_time)
            loss_func.set_dist_travelled(event.dist_travelled)

            info.lap_data = event

        elif isinstance(event, TrackInfo):
            loss_func.set_track_length(event.track_length)

            info.track_info = event

        elif isinstance(event, CarTelemetry):
            info.car_telemetry = event

    if ENV == "sim":
        sim.step(1/fps)