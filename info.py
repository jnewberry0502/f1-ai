import pygame

from f12019.telemetry import Location, LapData, TrackInfo, CarTelemetry


class Info:
    def __init__(self):
        self.location = Location()
        self.lap_data = LapData()
        self.track_info = TrackInfo()
        self.car_telemetry = CarTelemetry()

    def render(self, surface, **kwargs):
        myfont = pygame.font.SysFont("Arial", 40)
        time = myfont.render(f"{self.location.recieved}", 0, (255, 0, 0))
        position = myfont.render(f"{self.location.x:04f} {self.location.z:04f} {self.location.yaw:04f}", 0, (255, 0, 0))
        velocity = myfont.render(f"{self.location.dx:04f} {self.location.dz:04f}", 0, (255, 0, 0))
        lap_data = myfont.render(f"{self.lap_data.dist_travelled:04f} {self.lap_data.valid}", 0, (255, 0, 0))
        car_data = myfont.render(f"{self.car_telemetry.speed:02f} {self.car_telemetry.gear}", 0, (255, 0, 0))
        surface.blit(time, (0,0))
        surface.blit(position, (0,50))
        surface.blit(velocity, (0,100))
        surface.blit(lap_data, (0,150))
        surface.blit(car_data, (0,200))

        throttle_rect = pygame.Rect(600, 1000 - 100*self.car_telemetry.throttle , 50, 100*self.car_telemetry.throttle)
        brake_rect = pygame.Rect(700, 1000 - 100*self.car_telemetry.brake, 50, 100*self.car_telemetry.brake)

        if self.car_telemetry.steer >= 0:
            steer_rect = pygame.Rect(900, 900, 100*self.car_telemetry.steer, 50)
        else:
            steer_rect = pygame.Rect(900 + 100*self.car_telemetry.steer, 900, 100*abs(self.car_telemetry.steer), 50)

        pygame.draw.rect(surface, (0, 255, 0), throttle_rect)
        pygame.draw.rect(surface, (255, 0, 0), brake_rect)
        pygame.draw.rect(surface, (0, 0, 255), steer_rect)

        pygame.display.flip()
