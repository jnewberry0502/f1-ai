import pathlib

import numpy as np
from shapely.geometry import Point, LineString
from shapely.geometry.polygon import Polygon
import pygame
from rdp import rdp
from f12019.telemetry import Location

def point2surface(x, y, view_pos, view_scale, render_size):
    return ((x - view_pos[0]) + render_size[0]//2 * view_scale) / view_scale, ((y - view_pos[1]) + render_size[1]//2 * view_scale) / view_scale

class Track:
    """
        points_outter: 3xN array
        points_inner: 3xN array
    """
    tracks_dir = pathlib.Path(__file__).parent / "tracks"

    def __init__(self, points_file):
        self.points_file = Track.tracks_dir / points_file
        self.points_outer = np.zeros((0, 3))
        self.points_inner = np.zeros((0, 3))

    def _is_on_track(self, point):
        if isinstance(point, np.ndarray):
            point = Point(point)

        return self.outer_polygon.contains(point) and not self.inner_polygon.contains(point)

    def save(self):
        np.savez(self.points_file, outer=self.points_outer, inner=self.points_inner)

    @property
    def npz_file(self):
        if self.points_file.exists():
            return np.load(self.points_file)
        return {}

    def load(self):
        self.points_outer = self.npz_file["outer"]
        self.points_inner = self.npz_file["inner"]

        self.length = 5000
        self.start_pos = (-71.86482238769531, 499.97265625)
        self.start_yaw = np.pi / 4

        self.inner_polygon, self.outer_polygon = self.generate_polygons()


    def add_outer_point(self, loc: Location):
        self.points_outer = np.hstack([self.points_outer, loc.as_array])

    def add_inner_point(self, loc: Location):
        self.points_inner = np.hstack([self.points_inner, loc.as_array])

    def generate_polygons(self):
        inner_polygon = Polygon([[points[0], points[2]] for points in self.points_inner])
        outer_polygon = Polygon([[points[0], points[2]] for points in self.points_outer])

        return inner_polygon, outer_polygon

    @staticmethod
    def render_points(points, surface, view_pos, view_scale, render_size, marker_scale):
        color = (0, 255, 0)

        for i in range(-1, points.shape[0] - 1):
            x, y = point2surface(points[i, 0], points[i, 2], view_pos, view_scale, render_size)
            x2, y2 = point2surface(points[i+1, 0], points[i+1, 2], view_pos, view_scale, render_size)
            pygame.draw.aaline(surface, color, (x, y), (x2, y2), blend=2)

            center = (x+x2)/2, (y+y2)/2
            vec = (x2 - x, y2 - y)
            vec_norm = np.array([-vec[1], vec[0]])

            vec_norm /= np.linalg.norm(vec_norm)

            new_point = tuple(np.array(center) + np.array(vec_norm) * 50 * marker_scale)

            pygame.draw.aaline(surface, (0, 0, 255), center, new_point, blend=2)


    def render(self, surface, view_pos, view_scale, render_size, **kwargs):
        Track.render_points(self.points_outer, surface, view_pos, view_scale, render_size, 1)
        Track.render_points(self.points_inner, surface, view_pos, view_scale, render_size, -1)

    def simplify(self, epsilon=1.0):
        self.points_file = self.points_file.with_name(self.points_file.stem + '_simplified' + self.points_file.suffix)

        if f"outer" in self.npz_file:
            self.points_outer = self.npz_file[f"outer"]
        else:
            self.points_outer = rdp(self.points_outer, epsilon=epsilon)

        if f"outer" in self.npz_file:
            self.points_inner = self.npz_file[f"inner"]
        else:
            self.points_inner = rdp(self.points_inner, epsilon=epsilon)

        self.inner_polygon, self.outer_polygon = self.generate_polygons()

