import time

from f12019.telemetry import Telemetry, Location, LapData
from track import Track

last_dist = 100


def wait_for_new_lap(telem):
    global last_dist
    new_lap = False
    events = []
    while True:
        for event in telem.get():
            if isinstance(event, LapData):
                if event.dist_travelled > 0 and (last_dist - event.dist_travelled) > 1000:
                    # New lap detected
                    new_lap = True
                last_dist = event.dist_travelled
                if last_dist < 0:
                    last_dist = 5000
                if new_lap:
                    return events
            events.append(event)
        time.sleep(0.001)


def log_points(events):
    locs = []

    for event in events:
        if isinstance(event, Location):
            locs.append(event)

    return locs


if __name__ == "__main__":

    track = Track("australia.npz")

    telem = Telemetry()

    print("Cross the line, and then drive around the outside of the track")

    wait_for_new_lap(telem)

    print("---\n---\n---\nCrossed the line: Starting outside recording")

    for loc in log_points(wait_for_new_lap(telem)):
        track.add_outer_point(loc)

    print("---\n---\n---\nCross the line, and then drive around the inside of the track")

    wait_for_new_lap(telem)

    print("---\n---\n---\nCrossed the line: Starting inside recording")

    for loc in log_points(wait_for_new_lap(telem)):
        track.add_inner_point(loc)

    track.save()
