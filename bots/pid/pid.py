import numpy as np


class PID:
    def __init__(self, kp, ki, kd):
        self.setpoint = 0
        self.position = 0
        self.kp = kp
        self.ki = ki
        self.kd = kd

        self.error_sum = 0
        self.last_error = 0

    def set_setpoint(self, setpoint):
        self.setpoint = setpoint

    def set_position(self, position, dt):
        self.last_error = self.error
        self.position = position
        self.dt = dt
        self.error_sum += self.error * dt

    @property
    def error(self):
        return self.position - self.setpoint

    @property
    def output(self):
        P = self.kp * self.error
        I = self.ki * self.error_sum
        D = self.kd * (self.error - self.last_error) / self.dt
        return P + I + D
