from datetime import datetime

import numpy as np

from bots.pid.pid import PID


class PIDBot:
    def __init__(self):
        self.steer_pid = PID(0.3, 0, 0.1)
        self.last_time = datetime.now()
        self.drive_pid = PID(0.1, 0, 0)

    def drive(self, dists, speed):
        distance_left = dists[0]
        distance_right = dists[-1]
        distance_front = dists[len(dists) // 2]

        total_distance = distance_left + distance_right

        if distance_left < distance_right:
            distance_from_center = -(total_distance / 2 - distance_left)
        else:
            distance_from_center = total_distance / 2 - distance_right

        dt = (datetime.now() - self.last_time).total_seconds()

        target_speed = distance_front * 2.5

        self.drive_pid.set_position(speed, dt)
        self.steer_pid.set_position(distance_from_center, dt)

        self.drive_pid.set_setpoint(target_speed)

        steer = self.steer_pid.output

        out = -self.drive_pid.output

        if out > 0:
            throttle = out
            brake = 0
        else:
            throttle = 0
            brake = -out

        self.last_time = datetime.now()

        return throttle, brake, steer