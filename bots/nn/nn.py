from torch import nn

def create_model():
    return nn.Sequential(
        nn.BatchNorm1d(10),
        nn.Linear(10, 3)
    )