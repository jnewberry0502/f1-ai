import pathlib

import numpy as np
import torch

from bots.nn.nn import create_model


class NNBot:
    def __init__(self):
        self.model = create_model()
        self.model.load_state_dict(torch.load(pathlib.Path(__file__).parent / "model.pth"))
        self.model.eval()

    def drive(self, dists, speed):
        with torch.no_grad():
            x = [dists]
            x[0].append(speed)
            x = torch.Tensor(x)

            y = self.model(x)[0]

            return y[0], y[1], y[2]