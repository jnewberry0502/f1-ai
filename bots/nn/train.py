import pathlib

import numpy as np
import torch

from torch import nn, optim
from torch.utils.data import TensorDataset, DataLoader

from bots.nn.nn import create_model

if __name__ == "__main__":
    num_epochs = 5

    dir = pathlib.Path(__file__).parent

    model = create_model()

    X = np.load(dir / "X.npy")
    Y = np.load(dir / "Y.npy")

    tensor_x = torch.Tensor(X)
    tensor_y = torch.Tensor(Y)

    dataset = TensorDataset(tensor_x, tensor_y)
    dataloader = DataLoader(dataset, batch_size=150)

    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.005)

    for epoch in range(num_epochs):
        batch_losses = []

        data_iter = iter(dataloader)

        for batch in data_iter:
            optimizer.zero_grad()

            out = model(batch[0])

            loss = criterion(out, batch[1])

            batch_losses.append(loss.item())

            loss.backward()

            optimizer.step()

        print(np.mean(batch_losses) / dataloader.batch_size)

    torch.save(model.state_dict(), dir / "model.pth")