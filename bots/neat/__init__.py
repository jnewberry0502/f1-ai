import enum
import math
import random
from copy import deepcopy
from functools import partial

import numpy as np

from hashlib import sha256
import pygame

from simulation.car import clamp


def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))

def linear_rectified(x):
    return clamp(x, -1, 1)

class NodeType(enum.Enum):
    HIDDEN = 0
    BIAS = 1
    SENSOR = 2
    OUTPUT = 3


class Node:
    def __init__(self, num, t):
        self.num = num
        self.t = t
        self.output = 0
        self.input = 0

    def compute_output(self, sensors, connections, max_depth=10):
        if max_depth < 1:
            return
        for connection in connections:
            if connection.out_node is self and connection.enabled and not connection.processed:
                connection.in_node.compute_output(sensors, connections, max_depth-1)
                self.input += connection.in_node.output * connection.weight
                connection.processed = True

        if self.t == NodeType.BIAS:
            self.output = 1
        elif self.t == NodeType.SENSOR:
            self.output = sensors[self.num]
        else:
            self.output = linear_rectified(self.input)

    @staticmethod
    def find(nodes, query):
        for node in nodes:
            if node.num == query.num:
                return node
        return None


class Connection:
    def __init__(self, in_node, out_node, innovation_number, weight=0, enabled=True):
        self.weight = weight
        self.in_node = in_node
        self.out_node = out_node
        self.enabled = enabled
        self.innovation_number = innovation_number
        self.disabled_count = 0

    def randomize(self):
        if random.random() < 0.1:
            self.weight = random.uniform(-1, 1)
        else:
            self.weight += random.gauss(0, 1/50)
            self.weight = clamp(self.weight, -1, 1)

    @staticmethod
    def find(connections, query):
        for connection in connections:
            if connection.in_node == query.in_node and connection.out_node == query.out_node:
                return connection
        return None


class Genome:
    def __init__(self, sensor_nodes, output_nodes, bias_node=False):
        self.sensor_nodes = [Node(i, t=NodeType.SENSOR) for i in range(sensor_nodes)]
        self.output_nodes = [Node(i+len(self.sensor_nodes), t=NodeType.OUTPUT) for i in range(output_nodes)]
        self.hidden_nodes = []
        if bias_node:
            self.bias_node = [Node(len(self.sensor_nodes) + len(self.output_nodes), t=NodeType.BIAS)]
        else:
            self.bias_node = []
        self.connections = set() # prevent duplicate connections
        self.fitness = 0

    def get_connection(self, innovation_number):
        for x in self.connections:
            if x.innovation_number == innovation_number:
                return x

    @property
    def nodes(self):
        return self.sensor_nodes + self.hidden_nodes + self.output_nodes + self.bias_node

    @property
    def possible_input_nodes(self):
        return self.sensor_nodes + self.hidden_nodes + self.bias_node

    @property
    def possible_output_nodes(self):
        return self.hidden_nodes + self.output_nodes

    @property
    def node_to_idx(self):
        ret = {}
        for i, node in enumerate(self.nodes):
            ret[node] = i
        return ret

    def forward(self, sensors):
        for node in self.nodes:
            node.input = 0
            node.output = 0

        for connection in self.connections:
            connection.processed = False

        for node in self.possible_input_nodes:
            node.compute_output(sensors, self.connections)

        for node in self.output_nodes:
            node.compute_output(sensors, self.connections)

        return [x.output for x in self.output_nodes]

    def add_connection(self, connection):
        self.connections.add(connection)

    def mutate(self, innovation_number, node_add_chance, connection_add_chance, connection_enable_chance, connection_disable_chance, change_weights):
        if random.random() < node_add_chance and len(self.connections) > 0:
            connection = random.choice(list(self.connections))
            connection.enabled = False
            new_node = Node(len(self.nodes), NodeType.HIDDEN)
            self.hidden_nodes.append(new_node)
            self.add_connection(Connection(connection.in_node, new_node, innovation_number, 1))
            innovation_number += 1
            new_connection = Connection(new_node, connection.out_node, innovation_number, connection.weight)
            innovation_number += 1
            new_connection.randomize()
            self.add_connection(new_connection)
            new_connection.weight = connection.weight

        if random.random() < connection_add_chance:
            node_in = random.choice(self.possible_input_nodes)
            node_out = random.choice(self.possible_output_nodes)

            new_connection = Connection(node_in, node_out, innovation_number)
            old_connection = Connection.find(self.connections, new_connection)
            if not old_connection:
                self.add_connection(new_connection)
                new_connection.randomize()
                innovation_number += 1
            else:
                old_connection.enabled = True

        if len(self.connections) > 0:
            if random.random() < connection_enable_chance:
                random.choice(list(self.connections)).enabled = True
            if random.random() < connection_disable_chance:
                random.choice(list(self.connections)).enabled = False
            if random.random() < change_weights:
                for conn in self.connections:
                    conn.randomize()

        return innovation_number

    @staticmethod
    def connection_query(genome_1, genome_2, query):
        ret = []
        for connection_g1 in genome_1.connections:
            connection_g2 = next(filter(partial(query, connection_g1), genome_2.connections), None)
            if connection_g2 is not None:
                ret.append(connection_g1.innovation_number)
        return ret

    @staticmethod
    def disjoint(genome_1, genome_2):
        """connections that are in genome_1 but not genome_2"""
        return set([x.innovation_number for x in genome_1.connections]) - set(Genome.common(genome_1, genome_2))

    @staticmethod
    def common(genome_1, genome_2):
        """connections that are common between genome_1 and genome_2"""
        return Genome.connection_query(genome_1, genome_2, lambda x, y: x.innovation_number == y.innovation_number)

    @staticmethod
    def crossover(genome_1, genome_2):
        # Make sure the genomes are from the same family
        assert len(genome_1.sensor_nodes) == len(genome_2.sensor_nodes)
        assert len(genome_1.output_nodes) == len(genome_2.output_nodes)

        ret = Genome(len(genome_1.sensor_nodes), len(genome_1.output_nodes), len(genome_2.bias_node) > 0)
        connection_numbers = Genome.common(genome_1, genome_2)

        new_connections = []
        for conn in connection_numbers:
            new_connections.append(random.choice([genome_1, genome_2]).get_connection(conn))

        if genome_1.fitness > genome_2.fitness:
            for i in Genome.disjoint(genome_1, genome_2):
                new_connections.append(genome_1.get_connection(i))
        else:
            for i in Genome.disjoint(genome_2, genome_1):
                new_connections.append(genome_2.get_connection(i))

        for connection in new_connections:
            in_node = Node.find(ret.nodes, connection.in_node)
            out_node = Node.find(ret.nodes, connection.out_node)

            if in_node is None:
                in_node = Node(num=len(ret.nodes), t=NodeType.HIDDEN)
                ret.hidden_nodes.append(in_node)
            if out_node is None:
                out_node = Node(num=len(ret.nodes), t=NodeType.HIDDEN)
                ret.hidden_nodes.append(out_node)

            ret.add_connection(Connection(in_node, out_node, connection.innovation_number, connection.weight))

        return ret

    def render(self, surface, view_pos, view_scale, render_size, **kwargs):
        ren = pygame.Surface((1000, 1000))

        node_poses = {}

        for i, node in enumerate(self.sensor_nodes + self.bias_node):
            node_poses[node.num] = (100, (i+1) * (1000 / (len(self.sensor_nodes) + 2)))

        for i, node in enumerate(self.output_nodes):
            node_poses[node.num] = (900, (i+1) * (1000 / (len(self.output_nodes) + 2)))

        for i, node in enumerate(self.hidden_nodes):
            node_poses[node.num] = (500, (i+1) * (1000 / (len(self.hidden_nodes) + 2)))

        for conn in self.connections:
            if conn.enabled:
                pygame.draw.line(ren, (int(max(255*conn.weight, 0)), int(max(-255*conn.weight, 0)), 0, int(255*abs(conn.weight))), node_poses[conn.in_node.num], node_poses[conn.out_node.num], 15)

        for i, node in enumerate(self.nodes):
            pygame.draw.circle(ren, (int(max(255*node.output, 0)), int(max(-255*node.output, 0)), 255), node_poses[node.num], 20)

        surface.blit(pygame.transform.scale(ren, (render_size[0]//3, render_size[1]//3)), (666, 0))


class Population:
    def __init__(self, population_size, sensor_nodes, output_nodes, bias_node):
        self.population = []
        self.innovation_number = 0
        for i in range(population_size):
            self.population.append(Genome(sensor_nodes, output_nodes, bias_node=bias_node))

        self.mutate()

        self.survivor_ratio = 1/2

    def mutate(self):
        for pop in self.population:
            self.innovation_number = pop.mutate(**self.mutation_params)

    def step(self):
        ranked_population = sorted(self.population, key=lambda x: x.fitness, reverse=True)
        survivors = ranked_population[:int(len(self.population) * self.survivor_ratio)]
        children = []

        for i in range(int(len(self.population) * (1 - self.survivor_ratio))):
            children.append(Genome.crossover(random.choice(survivors), random.choice(survivors)))

        self.population = survivors + children
        self.mutate()

    @property
    def mutation_params(self):
        return {
            "innovation_number": self.innovation_number,
            "node_add_chance": 0.01,
            "connection_add_chance": 0.05,
            "connection_enable_chance": 0.1,
            "connection_disable_chance": 0.1,
            "change_weights": 0.8
        }