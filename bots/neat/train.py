import math
from datetime import datetime

import numpy as np

from bots.neat import Population
from car import Car
from f12019.telemetry import Location, LapData, TrackInfo, CarTelemetry
from info import Info
from lidar import Lidar
from ml.loss import Loss
from render import Render
from simulation.car import clamp
from simulation.controller import SimulationController
from simulation.simulation import Simulation
from simulation.telemetry import SimulationTelemetry
from track import Track

track = Track("australia.npz")
track.load()
track.simplify(epsilon=0.35)
track.save()

fps = 20
render = Render(fps, [], 0.1)

# -------- PARAMETERS -------- #
num_sensors = 7
population_size = 20
# ---------------------------- #

population = Population(population_size, num_sensors + 1, 3, bias_node=True)

info = Info()

generation = 0

max_time = 2

while True:
    generation += 1
    print(f"Generation: {generation}")

    total_time = 0

    alive_bois = list(range(len(population.population)))

    cars = [Car() for i in range(len(population.population))]

    sims = [Simulation(track) for i in range(len(population.population))]
    telems = [SimulationTelemetry(sims[i]) for i in range(len(population.population))]
    controllers = [SimulationController(sims[i]) for i in range(len(population.population))]

    lidars = [Lidar(track, num_sensors=num_sensors, fov_degrees=90) for i in range(len(population.population))]
    loss_funcs = [Loss() for i in range(len(population.population))]

    render.objects.clear()
    render.objects.append(track)
    render.objects.append(info)
    render.objects.extend(cars)

    best = (-1, 0)

    while total_time < max_time:
        render.objects.append(population.population[best[1]])
        render.objects.append(lidars[best[1]])
        render.view_pos = cars[best[1]].pos

        events = render.event_loop()
        render.render()
        for i in alive_bois:
            car_telemetry = None

            for event in telems[i].get():
                if isinstance(event, Location):
                    cars[i].pos = event.x, event.z
                    cars[i].yaw = event.yaw - np.pi / 2

                    lidars[i].generate_intersection_points(cars[i].pos, cars[i].yaw)

                    if i == best[1]:
                        info.location = event

                elif isinstance(event, LapData):
                    loss_funcs[i].set_lap_invalidated(not event.valid)
                    loss_funcs[i].set_time_elapsed(event.current_lap_time)
                    loss_funcs[i].set_dist_travelled(event.dist_travelled)

                elif isinstance(event, TrackInfo):
                    loss_funcs[i].set_track_length(event.track_length)

                elif isinstance(event, CarTelemetry):
                    car_telemetry = event

                    if i == best[1]:
                        info.car_telemetry = event

            vals = []

            for val in lidars[i].dists:
                vals.append(clamp(val / lidars[i].max_dist, -1, 1))
            vals.append(clamp(car_telemetry.speed / 250, -1, 1))

            throttle, brake, steer = population.population[i].forward(vals)

            controllers[i].throttle = throttle
            controllers[i].brake = brake
            controllers[i].steering = steer

            sims[i].step(0.1)

            population.population[i].fitness = loss_funcs[i].dist_travelled

            if population.population[i].fitness > best[0]:
                best = population.population[i].fitness, i

            if loss_funcs[i].lap_invalidated:
                alive_bois.remove(i)

        total_time += 0.1

        render.objects.pop()
        render.objects.pop()

    if len(alive_bois) > 0:
        max_time += 1
    else:
        max_time -= 1

    print(f"Best Pop: {population.population[best[1]].fitness}, Average Pop: {np.mean([population.population[i].fitness for i in range(len(population.population))])}")

    population.step()