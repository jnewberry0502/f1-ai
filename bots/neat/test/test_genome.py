import unittest

from bots.neat import Genome, Connection, Node, NodeType


class TestGenome(unittest.TestCase):
    def test_crossover(self):
        parent_1 = Genome(3, 1)
        node_1 = Node(num=1, t=NodeType.SENSOR)
        node_2 = Node(num=2, t=NodeType.SENSOR)
        node_3 = Node(num=3, t=NodeType.SENSOR)
        node_4 = Node(num=4, t=NodeType.HIDDEN)
        node_5 = Node(num=5, t=NodeType.OUTPUT)
        parent_1.nodes.append(node_1)
        parent_1.nodes.append(node_2)
        parent_1.nodes.append(node_3)
        parent_1.nodes.append(node_4)
        parent_1.nodes.append(node_5)
        parent_1.connections.add(Connection(node_1, node_4, innovation_number=1))
        parent_1.connections.add(Connection(node_2, node_4, innovation_number=2, enabled=False))
        parent_1.connections.add(Connection(node_3, node_4, innovation_number=3))
        parent_1.connections.add(Connection(node_2, node_5, innovation_number=4))
        parent_1.connections.add(Connection(node_5, node_4, innovation_number=5))
        parent_1.connections.add(Connection(node_1, node_5, innovation_number=8))

        parent_2 = Genome(3, 1)
        node_1 = Node(num=1, t=NodeType.SENSOR)
        node_2 = Node(num=2, t=NodeType.SENSOR)
        node_3 = Node(num=3, t=NodeType.SENSOR)
        node_4 = Node(num=4, t=NodeType.OUTPUT)
        node_5 = Node(num=5, t=NodeType.HIDDEN)
        node_6 = Node(num=6, t=NodeType.HIDDEN)
        parent_2.nodes.append(node_1)
        parent_2.nodes.append(node_2)
        parent_2.nodes.append(node_3)
        parent_2.nodes.append(node_4)
        parent_2.nodes.append(node_5)
        parent_2.nodes.append(node_6)

        parent_2.connections.add(Connection(node_1, node_4, innovation_number=1))
        parent_2.connections.add(Connection(node_2, node_4, innovation_number=2, enabled=False))
        parent_2.connections.add(Connection(node_3, node_4, innovation_number=3))
        parent_2.connections.add(Connection(node_2, node_5, innovation_number=4))
        parent_2.connections.add(Connection(node_5, node_4, innovation_number=5, enabled=False))
        parent_2.connections.add(Connection(node_5, node_6, innovation_number=6))
        parent_2.connections.add(Connection(node_6, node_4, innovation_number=7))
        parent_2.connections.add(Connection(node_3, node_5, innovation_number=9))
        parent_2.connections.add(Connection(node_1, node_6, innovation_number=10))

        offspring = Genome.crossover(parent_1, parent_2)

        print(offspring)

        self.assertEqual(len(offspring.connections), 9)