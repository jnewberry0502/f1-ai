import socket
from dataclasses import dataclass, field
from datetime import datetime

from f1_2019_telemetry.packets import unpack_udp_packet, PacketLapData_V1, PacketSessionData_V1, PacketMotionData_V1, \
    CarTelemetryData_V1, PacketCarTelemetryData_V1
import numpy as np

@dataclass
class TimedData:
    recieved: datetime = field(default_factory=datetime.now)

@dataclass
class Location(TimedData):
    x: float = 0.0
    y: float = 0.0
    z: float = 0.0

    dx: float = 0.0
    dy: float = 0.0
    dz: float = 0.0

    yaw: float = 0.0
    steering_angle: float = 0.0

    @property
    def as_array(self):
        numpy_array = np.array([self.x, self.y, self.z])
        numpy_array = np.expand_dims(numpy_array, axis=1)
        return numpy_array

@dataclass
class CarTelemetry(TimedData):
    speed: int = 0.0
    throttle: float = 0.0
    steer: float = 0.0
    brake: float = 0.0
    clutch: float = 0.0
    gear: int = 0.0
    engine_rpm: int = 0.0
    drs: int = 0.0

@dataclass
class LapData(TimedData):
    dist_travelled: float = 0
    lap_num: int = 0
    valid: bool = False
    current_lap_time: float = 0

@dataclass
class TrackInfo(TimedData):
    track_length: int = 0

@dataclass
class CarData(TimedData):
    speed: float = 0


class Telemetry:
    def __init__(self):
        self.udp_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.udp_socket.bind(('127.0.0.1', 20777), )
        self.udp_socket.setblocking(False)

    def get(self):
        events = []
        while True:
            try:
                udp_packet = self.udp_socket.recv(2048)
                packet = unpack_udp_packet(udp_packet)

                if isinstance(packet, PacketMotionData_V1):
                    loc = Location(
                        x=packet.carMotionData[0].worldPositionX,
                        y=packet.carMotionData[0].worldPositionY,
                        z=packet.carMotionData[0].worldPositionZ,
                        dx=packet.carMotionData[0].worldVelocityX,
                        dy=packet.carMotionData[0].worldVelocityY,
                        dz=packet.carMotionData[0].worldVelocityZ,
                        yaw=packet.carMotionData[0].yaw,
                        steering_angle=packet.frontWheelsAngle
                    )

                    events.append(loc)

                if isinstance(packet, PacketLapData_V1):
                    lap_data = LapData(
                        dist_travelled=packet.lapData[0].lapDistance,
                        lap_num=packet.lapData[0].currentLapNum,
                        valid=not packet.lapData[0].currentLapInvalid,
                        current_lap_time=packet.lapData[0].currentLapTime
                    )
                    events.append(lap_data)

                if isinstance(packet, PacketCarTelemetryData_V1):
                    car_data = CarTelemetry(
                        speed=packet.carTelemetryData[0].speed,
                        throttle=packet.carTelemetryData[0].throttle,
                        steer=packet.carTelemetryData[0].steer,
                        brake=packet.carTelemetryData[0].brake,
                        clutch=packet.carTelemetryData[0].clutch,
                        gear=packet.carTelemetryData[0].gear,
                        engine_rpm=packet.carTelemetryData[0].engineRPM
                    )
                    events.append(car_data)

                if isinstance(packet, PacketSessionData_V1):
                    track_info = TrackInfo(
                        track_length=packet.trackLength
                    )

                    events.append(track_info)


            except BlockingIOError as e:
                break

        return events
