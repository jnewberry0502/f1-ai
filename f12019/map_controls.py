from f12019.controller import Buttons, Controller

import time

if __name__ == '__main__':
    controller = Controller()

    for button in [Buttons.NAVIGATE_CANCEL]:
        print(str(button))
        time.sleep(2)
        controller.set_button(button, True)
        time.sleep(0.1)
        controller.set_button(button, False)


    """
    for axis in Axes:
        print(str(axis))
        time.sleep(5)
        controller.set_axis(axis, 0)
        time.sleep(0.1)
        controller.set_axis(axis, 0.5)
        time.sleep(0.1)
        controller.set_axis(axis, 1)
        time.sleep(0.1)
        controller.set_axis(axis, 0.5)
        time.sleep(0.1)
        controller.set_axis(axis, 0)

        time.sleep(2)
    

        controller.set_axis(axis, 0)
        time.sleep(0.1)
        controller.set_axis(axis, -0.5)
        time.sleep(0.1)
        controller.set_axis(axis, -1)
        time.sleep(0.1)
        controller.set_axis(axis, -0.5)
        time.sleep(0.1)
        controller.set_axis(axis, 0)
    """