import enum
import time

import pyvjoy

class Buttons(enum.IntEnum):
    DRS_ACTIVATE = 1
    UPSHIFT = 2
    DOWNSHIFT = 3
    PAUSE = 4
    CLUTCH = 5
    NAVIGATE_UP = 6
    NAVIGATE_DOWN = 7
    NAVIGATE_ACCEPT = 8
    NAVIGATE_CANCEL = 9


class Axes(enum.IntEnum):
    THROTTLE = pyvjoy.HID_USAGE_RX
    BRAKE = pyvjoy.HID_USAGE_RY
    STEERING = pyvjoy.HID_USAGE_Z

class Controller:
    def __init__(self):
        self.j = pyvjoy.VJoyDevice(1)

    def _set_button(self, button: Buttons, pressed: bool):
        self.j.set_button(button, pressed)

    def _set_axis(self, axis: Axes, value: float):
        MAX = 32768
        self.j.set_axis(axis, int(value * MAX / 2 + MAX / 2))

    def reset(self):
        self.set_button(Buttons.PAUSE, True)
        time.sleep(0.01)
        self.set_button(Buttons.PAUSE, False)

        time.sleep(1)

        self.set_button(Buttons.NAVIGATE_ACCEPT, True)
        time.sleep(0.01)
        self.set_button(Buttons.NAVIGATE_ACCEPT, False)

    def set_throttle(self, value):
        self._set_axis(Axes.THROTTLE, value)

    def set_brake(self, value):
        self._set_axis(Axes.BRAKE, value)

    def set_steering(self, value):
        self._set_axis(Axes.STEERING, value)

    def __del__(self):
        for axis in Axes:
            self._set_axis(axis, 0)