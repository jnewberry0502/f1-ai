import unittest
from track import Track
from f12019.telemetry import Location
import numpy as np


class TestTrack(unittest.TestCase):
    def test_save(self):
        inside = (np.array([(1.0, 1.1, 0), (2, 2.25, 0), (3, 1, 0)]) * 50)
        outside = (np.array([(0, 0, 0), (3, 3, 0), (6, 0, 0)]) * 50)

        t = Track("../tracks/demo")
        for i in range(len(inside)):
            l = Location(inside[i])

            l.x, l.y, l.z = inside[i]
            t.add_inner_point(l)

            l.x, l.y, l.z = outside[i]
            t.add_outer_point(l)

        t.save()


if __name__ == '__main__':
    unittest.main()
