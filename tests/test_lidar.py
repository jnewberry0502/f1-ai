import unittest
from lidar import Lidar
from track import Track


class TestLidar(unittest.TestCase):

    def test_sensor_generation(self):
        t = Track("../tracks/demo.npz")
        t.load()

        l = Lidar(t)


if __name__ == '__main__':
    unittest.main()
