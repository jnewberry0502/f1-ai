import pygame.draw
from shapely.coords import CoordinateSequence
from shapely.geometry import Point, MultiPoint
from shapely.geometry import LineString
from shapely.strtree import STRtree

from track import Track, point2surface
import numpy as np

class Lidar:

    def __init__(self, track: Track, num_sensors=9, fov_degrees=180, max_dist=500):
        bound = fov_degrees / 2
        increment = fov_degrees / (num_sensors - 1)
        self.sensor_angles = np.arange(-bound, bound + increment, increment)

        self.end_points = []
        self.dists = []
        self.track = track
        self.max_dist = max_dist

    @staticmethod
    def _intersection_to_list(intersection):
        points = []
        if isinstance(intersection, Point):
            points.append(np.array(intersection))
        elif isinstance(intersection, LineString) or isinstance(intersection, MultiPoint):
            if not intersection.is_empty:
                for point in list(intersection):
                    points.append(np.array(point))
        else:
            raise Exception(f"Unhandled intersection type: {type(intersection)}")

        return points

    def generate_intersection_points(self, view_pos: np.ndarray, yaw: float):
        car_position = np.array(view_pos)

        # TODO: Implement negative distances if off track
        on_track = self.track._is_on_track(car_position)
        multiplier = 1 if on_track else -1

        self.end_points = []
        self.dists = []

        for angle in self.sensor_angles:
            angle_rad = np.radians(angle)

            relative_angle = angle_rad + yaw

            angle_vector = np.array([
                np.cos(relative_angle),
                -np.sin(relative_angle)
            ])

            end_point = self.max_dist * angle_vector + car_position

            laser = LineString([car_position, end_point])
            outer_intersections = self.track.outer_polygon.boundary.intersection(laser)
            inner_intersections = self.track.inner_polygon.boundary.intersection(laser)

            outer_intersection_list = self._intersection_to_list(outer_intersections)
            inner_intersection_list = self._intersection_to_list(inner_intersections)

            if len(outer_intersection_list) == 0:
                outer_intersection_list.append(end_point)

            if len(inner_intersection_list) == 0:
                inner_intersection_list.append(end_point)

            outer_intersection_array = np.array(outer_intersection_list)
            inner_intersection_array = np.array(inner_intersection_list)

            outer_dists = np.linalg.norm(outer_intersection_array - car_position, axis=1)
            inner_dists = np.linalg.norm(inner_intersection_array - car_position, axis=1)

            closest_outer_idx = np.argmin(outer_dists)
            closest_inner_idx = np.argmin(inner_dists)

            if outer_dists[closest_outer_idx] < inner_dists[closest_inner_idx]:
                self.end_points.append(outer_intersection_array[closest_outer_idx])
                self.dists.append(multiplier * outer_dists[closest_outer_idx])
            else:
                self.end_points.append(inner_intersection_array[closest_inner_idx])
                self.dists.append(multiplier * inner_dists[closest_inner_idx])

    def render(self, surface, view_pos, view_scale, render_size, **kwargs):

        center = np.array(render_size) // 2

        for end_point in self.end_points:
            new_point = point2surface(end_point[0], end_point[1], view_pos, view_scale, render_size)

            pygame.draw.aaline(surface, (255, 255, 20), center, new_point)


    def print_dists(self):

        print("Lidar Distances:")

        for i, dist in enumerate(self.dists):
            print(f"\tAngle: {round(self.sensor_angles[i], 3)}   Dist:  {round(dist, 4)}")
