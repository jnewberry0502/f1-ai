from os import environ

import numpy as np

environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

import pygame
from pygame import QUIT, HWSURFACE

pygame.init()

class Render:
    def __init__(self, fps, objects, view_scale = 0.3):
        self.WINDOW_SIZE = 500, 500
        self.VIEW_SCALE = view_scale

        self.WORLD_SIZE = 1000, 1000

        self.screen = self.create_screen()

        self.clock = pygame.time.Clock()

        self.surface = pygame.surface.Surface(self.WORLD_SIZE)

        self.center = np.array(self.WORLD_SIZE)/2

        self.view_pos = self.center
        self.fps = fps

        self.objects = objects


    @property
    def render_params(self):
        return {
            "view_pos": self.view_pos,
            "view_scale": self.VIEW_SCALE,
            "render_size": self.WORLD_SIZE,
            "center": (self.WORLD_SIZE[0]//2, self.WORLD_SIZE[1]//2)
        }

    def update(self, view_pos):
        self.view_pos = view_pos

    def render(self):
        self.surface.fill((0,0,0))

        for object in self.objects:
            object.render(self.surface, **self.render_params)

        self.screen.blit(pygame.transform.scale(self.surface, self.WINDOW_SIZE), (0, 0))

        pygame.display.update()
        pygame.display.flip()

        self.clock.tick(self.fps)

    def create_screen(self):
        return pygame.display.set_mode(self.WINDOW_SIZE, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)

    def event_loop(self):
        events = []
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.display.quit()
            elif event.type == pygame.VIDEORESIZE:
                self.WINDOW_SIZE = event.dict['size']
                self.screen = self.create_screen()
                self.render()
            else:
                events.append(event)

        return events




