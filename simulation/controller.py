from simulation.car import clamp


class SimulationController:
    def __init__(self, simulation):
        self.simulation = simulation
        self._throttle = 0
        self._brake = 0
        self._steering = 0

    @property
    def throttle(self):
        return self._throttle

    @throttle.setter
    def throttle(self, val):
        self._throttle = clamp(val, 0, 1)
        self.update()

    @property
    def brake(self):
        return self._brake

    @brake.setter
    def brake(self, val):
        self._brake = clamp(val, 0, 1)
        self.update()

    @property
    def steering(self):
        return self._steering

    @steering.setter
    def steering(self, val):
        self._steering = clamp(val, -1, 1)
        self.update()

    def update(self):
        self.simulation.car.throttle = self.throttle
        self.simulation.car.brake = self.brake
        self.simulation.car.steering = self.steering

    def reset(self):
        self.simulation.reset()
