import math

import numpy as np

def clamp(val, min_val, max_val):
    return max(min(val, max_val), min_val)

class Car:
    def __init__(self, world, pos, yaw):
        self.position = np.array(pos)
        self.yaw = yaw
        self.reset()

        self.c_drag = 2
        self.c_rr = self.c_drag * 30

        self.mass = 500

        self.steering_angle = 0

        self.steering_lock = np.radians(17)

        self.gear = 0

        self.L = 8

        self.steering_turn_speed = 0.25 # Time to turn from 0 to steering_lock

        self.distance_travelled = 0

    def reset(self):
        self.velocity = np.array([0,0], dtype=np.float64)
        self.throttle = 0
        self.brake = 0
        self.steering = 0
        self.steering_angle = 0
        self.distance_travelled = 0

    @property
    def heading(self):
        heading = np.array([math.sin(self.yaw), math.cos(self.yaw)])
        return heading / np.linalg.norm(heading)

    @property
    def f_traction(self):
        return self.heading * self.engine_force - self.heading * self.braking_force

    @property
    def engine_force(self):
        return self.throttle * 20000

    @property
    def braking_force(self):
        if np.dot(self.velocity, self.heading) > 0:
            return self.brake * 4000
        return 0

    @property
    def f_drag(self):
        return -self.c_drag * self.velocity * self.speed

    # Rolling Resistance
    @property
    def f_rr(self):
        return -self.c_rr * self.velocity

    @property
    def speed(self):
        return np.linalg.norm(self.velocity)

    @property
    def acceleration(self):
        return (self.f_traction + self.f_rr + self.f_drag) / self.mass

    def step(self, dt):
        dv = self.acceleration * dt
        self.velocity += dv

        self.position += self.velocity * dt

        desired_angle = self.steering_lock * self.steering

        max_turn = dt * 1/self.steering_turn_speed * self.steering_lock

        to_move = clamp((desired_angle - self.steering_angle), -max_turn, max_turn)

        self.steering_angle += to_move

        if self.steering_angle == 0:
            dyaw = 0
        else:
            R = self.L / math.sin(self.steering_angle)

            dyaw = self.speed / R * dt

        self.yaw += -dyaw

        self.velocity = self.heading * self.speed

        self.distance_travelled += self.speed * dt