import math

from simulation.simulation import Simulation
from f12019.telemetry import Location, CarTelemetry, TrackInfo, LapData


class SimulationTelemetry:
    def __init__(self, sim: Simulation):
        self.sim = sim

    def get(self):
        events = []
        loc = Location(
            x=self.sim.car.position[0],
            y=0,
            z=self.sim.car.position[1],
            dx=self.sim.car.velocity[0],
            dy=0,
            dz=self.sim.car.velocity[1],
            yaw=self.sim.car.yaw,
            steering_angle=self.sim.car.steering_angle
        )
        events.append(loc)

        car_telem = CarTelemetry(
            speed=self.sim.car.speed,
            throttle=self.sim.car.throttle,
            brake=self.sim.car.brake,
            steer=self.sim.car.steering
        )

        events.append(car_telem)

        track_info = TrackInfo(
            track_length=self.sim.track.length,
        )

        events.append(track_info)

        lap_data = LapData(
            valid=self.sim.valid_lap,
            current_lap_time=self.sim.time_elapsed,
            dist_travelled=self.sim.distance_travelled()
        )

        events.append(lap_data)

        return events