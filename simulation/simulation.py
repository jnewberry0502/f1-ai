import numpy as np
from shapely.geometry import Point

from simulation.world import World
from simulation.car import Car
from simulation.ground import NaiveGround

class Simulation:
    def __init__(self, track):
        self.world = World((1000, 1000))
        self.track = track

        self.car = Car(self.world, self.track.start_pos, self.track.start_yaw)
        self.ground = NaiveGround(self.world)

        self.time_elapsed = 0

        self.valid_lap = True

    def reset(self):
        self.car.reset()
        self.car.position = self.track.start_pos
        self.car.yaw = self.track.start_yaw
        self.time_elapsed = 0

    def step(self, dt):
        self.car.step(dt)

        self.time_elapsed += dt

        if not self.on_track():
            self.valid_lap = False

    def on_track(self):
        return self.track._is_on_track(Point(self.car.position))

    def distance_travelled(self):
        return self.car.distance_travelled